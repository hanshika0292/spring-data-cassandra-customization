package com.sample.customer.repositories;

import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.sample.customer.mapper.JsonEntitySerializer;
import com.sample.customer.model.Customer;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.mapping.model.IllegalMappingException;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

/**
 * Created by hanshika on 12/9/16.
 */
@Transactional
public class CustomerRepositoryImpl implements CustomerRepositoryCustom<Customer>{

    private CassandraOperations cassandraOperations;

    private String keyspace;

    private final String TABLE_NAME = "customer_master";

    @Inject
    CustomerRepositoryImpl(CassandraOperations cassandraOperations) {
        this.cassandraOperations = cassandraOperations;
    }

    @Override
    public void addKeyspace(String keyspace) {
        this.keyspace = keyspace;
        cassandraOperations.execute("USE "+keyspace);
    }

    @Override
    public <S extends Customer> S save(S entity) {
        CommonMethods<Customer> customerCommonMethods = new CommonMethods<>();
        cassandraOperations.execute(QueryBuilder
                .insertInto(this.keyspace,TABLE_NAME)
                .json(customerCommonMethods.convertToJson(entity)));
        return entity;
    }
}
