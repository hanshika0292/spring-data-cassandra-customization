package com.sample.customer.repositories;

import com.datastax.driver.core.Statement;
import com.datastax.driver.core.querybuilder.Insert;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.sample.customer.annotations.JsonEntity;
import com.sample.customer.model.Client;
import com.sample.customer.model.ClientPrimaryKey;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;
import org.springframework.data.cassandra.mapping.Table;
/**
 * Created by hanshika on 12/9/16.
 */
@Transactional
public class ClientRepositoryImpl implements ClientRepositoryCustom<Client>{

    private CassandraOperations cassandraOperations;

    private String keyspace;

    private final String TABLE_NAME;
    private final String PARTITION_KEY_NAME = "org_client_id";
    private final String CLUSTERING_KEY_NAME = "last_updated_tstamp";

    @Inject
    ClientRepositoryImpl(CassandraOperations cassandraOperations) {
        TABLE_NAME = Client.class.getAnnotation(Table.class).value();
        this.cassandraOperations = cassandraOperations;
    }

    @Override
    public void addKeyspace(String keyspace) {
        this.keyspace = keyspace;
        cassandraOperations.execute("USE "+keyspace);
    }

    @Override
    public <S extends Client> S save(String keyspace,S entity) {
        CommonMethods<Client> clientCommonMethods = new CommonMethods<>();
        cassandraOperations.execute(QueryBuilder
                .insertInto(keyspace,TABLE_NAME)
                .json(clientCommonMethods.convertToJson(entity)));
        return entity;
    }

    @Override
    public <S extends Client> S save(S entity) {
        throw new UnsupportedOperationException("This function is supported with keyspace parameter only");
    }

    @Override
    public Client findById(String keyspace, ClientPrimaryKey id) {
        Statement select = QueryBuilder.select()
                .from(keyspace, TABLE_NAME)
                .where(QueryBuilder.eq(PARTITION_KEY_NAME, id.getOrg_client_id()))
                .and(QueryBuilder.eq(CLUSTERING_KEY_NAME,id.getLast_updated_tstamp()));
        Client client = cassandraOperations.selectOne(select.toString(),Client.class);
        return client;
    }

    @Override
    public Client findById(Long id) {
        throw new UnsupportedOperationException("This function is supported with keyspace parameter only");
    }

    @Override
    public void deleteAll(String keyspace) {
        cassandraOperations.execute(QueryBuilder
                .truncate(keyspace,TABLE_NAME));
    }

    @Override
    public void deleteAll() {
        throw new UnsupportedOperationException("This function is supported with keyspace parameter only");
    }

    @Override
    public Iterable<Client> findAll(String keyspace) {
        Select select = QueryBuilder.select()
                .from(keyspace,TABLE_NAME);
        List<Client> clientList = cassandraOperations.select(select,Client.class);
        return clientList;
    }

    @Override
    public Iterable<Client> findAll() {
        throw new UnsupportedOperationException("This function is supported with keyspace parameter only");
    }

}
