package com.sample.customer.repositories;

import com.sample.customer.model.Users;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

/**
 * Created by hanshika on 12/9/16.
 */
@Transactional
public class UsersRepositoryImpl implements UsersRepositoryCustom<Users> {
    private CassandraOperations cassandraOperations;

    @Inject
    UsersRepositoryImpl(CassandraOperations cassandraOperations) {
        this.cassandraOperations = cassandraOperations;
    }

    @Override
    public void addKeyspace(String keyspace) {
        cassandraOperations.execute("USE "+keyspace);
    }

//    @Override
    public <S extends Users> S save(S entity) {
        return null;
    }

}
