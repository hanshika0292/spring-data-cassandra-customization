package com.sample.customer.repositories;

import com.sample.customer.model.Customer;
import com.sample.customer.model.Users;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.cassandra.repository.TypedIdCassandraRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

/**
 * Created by hanshika on 12/9/16.
 */
public interface UsersRepository extends TypedIdCassandraRepository<Users, String>,UsersRepositoryCustom<Users> {

    @Query("Select * from sample.users where \"userId\"=?0")
    Users findById(String id);

    @Query("Select * from sample.users")
    Iterable<Users> findAll();

    @Query("Truncate sample.users")
    void deleteAll();

}