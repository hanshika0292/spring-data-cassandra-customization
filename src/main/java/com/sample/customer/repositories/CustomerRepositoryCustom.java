package com.sample.customer.repositories;

import org.springframework.data.repository.NoRepositoryBean;

/**
 * Created by hanshika on 12/9/16.
 */
@NoRepositoryBean
public interface CustomerRepositoryCustom<T> {

    void addKeyspace(String keyspace);

    <S extends T> S save(S entity);
}
