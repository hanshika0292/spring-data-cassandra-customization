package com.sample.customer.repositories;

/**
 * Created by hanshika on 8/26/16.
 */

import com.sample.customer.model.Client;
import org.springframework.data.cassandra.repository.TypedIdCassandraRepository;

public interface ClientRepository extends TypedIdCassandraRepository<Client, String>,ClientRepositoryCustom<Client> {


}