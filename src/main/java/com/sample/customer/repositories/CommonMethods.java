package com.sample.customer.repositories;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.sample.customer.mapper.JsonEntitySerializer;
import com.sample.customer.model.Client;
import org.json.JSONObject;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.mapping.model.IllegalMappingException;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by hanshika on 13/9/16.
 */
public class CommonMethods<T>{

    public <S extends T> String convertToJson(S entity){
        ObjectMapper mapper = new ObjectMapper();
        String data,data1 = null;
        Object key = null;
        SimpleModule module = new SimpleModule();
        module.addSerializer(entity.getClass(), new JsonEntitySerializer());
        mapper.registerModule(module);
        Field[] fields = entity.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(PrimaryKey.class)) {
                key = ((Client) entity).getClientPrimaryKey();
            }
        }
        try {
            data = mapper.writeValueAsString(entity);
            if(key!=null) {
                data1 = mapper.writeValueAsString(key);
            }
        } catch (JsonProcessingException e) {
            throw new IllegalMappingException("Unable to convert object to JSON", e);
        }
        return add2JsonStirngs(data,data1);
    }

    private String add2JsonStirngs(String data, String data1) {
        JSONObject o1 = new JSONObject(data);
        JSONObject mergedObj = new JSONObject();
        Iterator i1 = o1.keys();
        String tmp_key;
        if(data1!=null) {
            JSONObject o2 = new JSONObject(data1);
            Iterator i2 = o2.keys();
            while(i2.hasNext()) {
                tmp_key = (String) i2.next();
                mergedObj.put(tmp_key, o2.get(tmp_key));
            }
        }
        while(i1.hasNext()) {
            tmp_key = (String) i1.next();
            mergedObj.put(tmp_key, o1.get(tmp_key));
        }
        return mergedObj.toString();
    }
}
