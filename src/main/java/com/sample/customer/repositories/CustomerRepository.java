package com.sample.customer.repositories;

import com.sample.customer.model.Customer;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.cassandra.repository.TypedIdCassandraRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

/**
 * Created by hanshika on 9/9/16.
 */
public interface CustomerRepository extends TypedIdCassandraRepository<Customer, String>,CustomerRepositoryCustom<Customer> {

    @Query("Select * from customer.customer_master where customer_id=?0")
    Customer findById(UUID id);

    @Query("Select * from customer.customer_master")
    Iterable<Customer> findAll();

    @Query("Truncate customer.customer_master")
    void deleteAll();

}