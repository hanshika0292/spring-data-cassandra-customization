package com.sample.customer.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.UUID;

/**
 * Created by hanshika on 12/9/16.
 */
@NoRepositoryBean
public interface UsersRepositoryCustom<T> {

    void addKeyspace(String keyspace);

//    <S extends T> S save(S entity);
}
