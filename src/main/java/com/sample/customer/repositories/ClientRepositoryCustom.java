package com.sample.customer.repositories;

import com.sample.customer.model.Client;
import com.sample.customer.model.ClientPrimaryKey;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Created by hanshika on 12/9/16.
 */
@NoRepositoryBean
public interface ClientRepositoryCustom<T> {

    void addKeyspace(String keyspace);

    <S extends T> S save(String keyspace,S entity);

    <S extends T> S save(S entity);

    Client findById(String keyspace, ClientPrimaryKey id);

    Client findById(Long id);

    void deleteAll(String keyspace);

    void deleteAll();

    Iterable<Client> findAll(String keyspace);

    Iterable<Client> findAll();

}

