package com.sample.customer.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.cassandra.core.Ordering;
import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by hanshika on 14/9/16.
 */

@PrimaryKeyClass
public class ClientPrimaryKey implements Serializable{

    @PrimaryKeyColumn(name = "org_client_id",ordinal = 0,type = PrimaryKeyType.PARTITIONED)
    @JsonProperty(value = "org_client_id", required = true)
    private Long org_client_id;

    @PrimaryKeyColumn(name = "last_updated_tstamp",ordinal = 1,type = PrimaryKeyType.CLUSTERED,ordering = Ordering.DESCENDING)
    @JsonProperty(value = "last_updated_tstamp")
    private Date last_updated_tstamp;

    public Long getOrg_client_id() {
        return org_client_id;
    }

    public void setOrg_client_id(Long org_client_id) {
        this.org_client_id = org_client_id;
    }

    public Date getLast_updated_tstamp() {
        return last_updated_tstamp;
    }

    public void setLast_updated_tstamp(Date last_updated_tstamp) {
        this.last_updated_tstamp = last_updated_tstamp;
    }
}
