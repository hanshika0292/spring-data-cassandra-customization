package com.sample.customer.model;

/**
 * Created by hanshika on 8/26/16.
 */

import com.datastax.driver.mapping.annotations.ClusteringColumn;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sample.customer.annotations.CatchAll;
import com.sample.customer.annotations.JsonEntity;
import org.springframework.data.annotation.Id;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import java.util.Date;
import java.util.Map;

@Table(value = "client_master")
@JsonEntity
public class Client {

    @PrimaryKey
    private ClientPrimaryKey clientPrimaryKey;

    @Column(value = "first_name")
    @JsonProperty(value = "first_name")
    private String firstName;

    @Column(value = "last_name")
    @JsonProperty(value = "last_name")
    private String lastName;

    public Client(long org_client_id, String first_name, String last_name, Date last_updated_tstamp, Map<String, Object> clientProps) {
        this.clientPrimaryKey = new ClientPrimaryKey();
        this.clientPrimaryKey.setOrg_client_id(org_client_id);
        this.firstName = first_name;
        this.lastName = last_name;
        this.clientPrimaryKey.setLast_updated_tstamp(last_updated_tstamp);
    }

    @Override
    public String toString() {
        return "Client{" +
                "org_client_id=" + clientPrimaryKey.getOrg_client_id() +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", last_updated_tstamp=" + clientPrimaryKey.getLast_updated_tstamp() +
                ", clientProps=" + clientProps +
                '}';
    }

    @CatchAll
    private Map<String, Object> clientProps;
    
    public Client() {
        this.clientPrimaryKey = new ClientPrimaryKey();
    }

    public Client(Long id, String firstName, String lastName) {
        this.clientPrimaryKey = new ClientPrimaryKey();
        this.clientPrimaryKey.setOrg_client_id(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.clientPrimaryKey.setLast_updated_tstamp(new Date());
    }

    public Client(Long org_client_id, String firstName, String lastName, Map<String, Object> clientProps) {
        this.clientPrimaryKey = new ClientPrimaryKey();
        this.clientPrimaryKey.setOrg_client_id(org_client_id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.clientPrimaryKey.setLast_updated_tstamp(new Date());
        this.clientProps = clientProps;
    }

    public ClientPrimaryKey getClientPrimaryKey() {
        return clientPrimaryKey;
    }

    public void setClientPrimaryKey(ClientPrimaryKey clientPrimaryKey) {
        this.clientPrimaryKey = clientPrimaryKey;
    }

    public Map<String, Object> getClientProps() {
        return clientProps;
    }

    public void setClientProps(Map<String, Object> clientProps) {
        this.clientProps = clientProps;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}