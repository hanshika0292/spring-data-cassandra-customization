package com.sample.customer.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sample.customer.annotations.CatchAll;
import com.sample.customer.annotations.JsonEntity;
import org.springframework.cassandra.core.Ordering;
import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.mapping.Table;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * Created by hanshika on 9/9/16.
 */

@Table(value = "customer_master")
@JsonEntity
public class Customer {

    @PrimaryKeyColumn(name = "customer_id",ordinal = 0,type = PrimaryKeyType.PARTITIONED)
    @JsonProperty(value = "customer_id", required = true)
    private UUID customerId;

    @Column(value = "first_name")
    @JsonProperty(value = "first_name")
    private String firstName;

    @Column(value = "last_name")
    @JsonProperty(value = "last_name")
    private String lastName;

    @PrimaryKeyColumn(name = "email",ordinal = 1,type = PrimaryKeyType.CLUSTERED,ordering = Ordering.DESCENDING)
    @JsonProperty(value = "email")
    private String email;

    @CatchAll
    private Map<String, Object> otherCustomerProps;

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", otherCustomerProps=" + otherCustomerProps +
                '}';
    }

    public Customer() {
    }

    public Customer(UUID id, String firstName, String lastName,String email) {
        this.customerId = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email=email;
    }

    public Customer(UUID org_client_id, String firstName, String lastName, String email, Map<String, Object> clientProps) {
        this.customerId = org_client_id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email=email;
        this.otherCustomerProps = clientProps;
    }

    public UUID getCustomerId() {
        return customerId;
    }

    public void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Map<String, Object> getOtherCustomerProps() {
        return otherCustomerProps;
    }

    public void setOtherCustomerProps(Map<String, Object> otherCustomerProps) {
        this.otherCustomerProps = otherCustomerProps;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
