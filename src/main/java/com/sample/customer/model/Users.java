package com.sample.customer.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sample.customer.annotations.JsonEntity;
import org.springframework.context.annotation.Primary;
import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

/**
 * Created by hanshika on 12/9/16.
 */

@Table(value = "users")
public class Users {

    @PrimaryKey
    @Column(value = "userId")
    String userId;

    @Column(value = "city")
    String city;

    @Column(value = "first_name")
    String firstName;

    @Column(value = "last_name")
    String lastName;

    public Users(String userId, String city, String firstName, String lastName) {
        this.userId = userId;
        this.city = city;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Users{" +
                "userId=" + userId +
                ", city='" + city + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
