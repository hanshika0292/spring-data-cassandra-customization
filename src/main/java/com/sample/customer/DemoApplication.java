package com.sample.customer;

import com.sample.customer.model.Client;
import com.sample.customer.model.ClientPrimaryKey;
import com.sample.customer.model.Customer;
import com.sample.customer.model.Users;
import com.sample.customer.repositories.ClientRepository;
import com.sample.customer.repositories.CustomerRepository;
import com.sample.customer.repositories.UsersRepository;
import io.netty.util.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.*;

/**
 * Created by hanshika on 9/9/16.
 */

@SpringBootApplication
@EnableAsync
public class DemoApplication implements CommandLineRunner{


    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private UsersRepository usersRepository;


    @Override
    public void run(String... args) {

        (new Thread(new CustomerThread("customer"))).start();
        (new Thread(new UsersThread("sample"))).start();
        (new Thread(new ClientThread("iris"))).start();
        (new Thread(new Client2Thread("iris1"))).start();

//        System.exit(0);

    }

    class ClientThread implements Runnable {

        private final String keyspace;

        public ClientThread(String keyspace) {
            this.keyspace = keyspace;
        }

        @Override
        public void run() {

            Map<String,Object> propMapClient = new HashMap<>();
            propMapClient.put("city","SFO");

            Map<String,Object> propMapClient2 = new HashMap<>();
            propMapClient2.put("city", "IAD");

            Map<String,Object> propMapClient3 = new HashMap<>();
            propMapClient3.put("city","LA");
            Map<Date,Map<String,Object>> client_health_care_contacts_map = new HashMap<>();
            Map<String, Object> contactsMap1 = new HashMap<>();
            contactsMap1.put("name"," hanshika");
            contactsMap1.put("hospital","care");
            contactsMap1.put("type","heart care");
            contactsMap1.put("id_number","4512");
            contactsMap1.put("home_phone","8465151511");
            contactsMap1.put("last_appt_date",new Date());
            client_health_care_contacts_map.put(new Date(),contactsMap1);
            propMapClient3.put("client_health_care_contacts", client_health_care_contacts_map);
            //Third thread accessing keyspace : iris;

            System.out.println("Thread-1");

            clientRepository.addKeyspace(keyspace);

            clientRepository.deleteAll(keyspace);
            // save a couple of customers
            Date date1 = new Date();
            clientRepository.save(keyspace,new Client(1L, "Alice", "Smith",date1, propMapClient));
            clientRepository.save(keyspace,new Client(2L, "Bob", "Smith", propMapClient2));
            clientRepository.save(keyspace,new Client(5L,"Jana","NaJana",propMapClient3));

            // fetch all customers
            System.out.println("Clients found with findAll():");
            System.out.println("-------------------------------");
            for (Client customer : clientRepository.findAll(keyspace)) {
                System.out.println(customer);
            }

//            System.out.println(clientRepository.findById(1L));
            System.out.println("Select by one: ");
            ClientPrimaryKey id = new ClientPrimaryKey();
            id.setOrg_client_id(1L);
            id.setLast_updated_tstamp(date1);
            System.out.println(clientRepository.findById(keyspace,id));
        }
    }

    class Client2Thread implements Runnable {

        private final String keyspace;

        public Client2Thread(String keyspace) {
            this.keyspace = keyspace;
        }

        @Override
        public void run() {

            Map<String,Object> propMapClient = new HashMap<>();
            propMapClient.put("city","KOL");

            Map<String,Object> propMapClient2 = new HashMap<>();
            propMapClient2.put("city", "MUM");

            Map<String,Object> propMapClient3 = new HashMap<>();
            propMapClient3.put("city","HYD");
            Map<Date,Map<String,Object>> client_health_care_contacts_map = new HashMap<>();
            Map<String, Object> contactsMap1 = new HashMap<>();
            contactsMap1.put("name"," hanshika");
            contactsMap1.put("hospital","care");
            contactsMap1.put("type","heart care");
            contactsMap1.put("id_number","4512");
            contactsMap1.put("home_phone","8465151511");
            contactsMap1.put("last_appt_date",new Date());
            client_health_care_contacts_map.put(new Date(),contactsMap1);
            propMapClient3.put("client_health_care_contacts", client_health_care_contacts_map);
            //Third thread accessing keyspace : iris;

            System.out.println("Thread-2");

            clientRepository.addKeyspace(keyspace);

            clientRepository.deleteAll(keyspace);
            // save a couple of customers

            Date date1 = new Date();
            clientRepository.save(keyspace,new Client(1L, "Hanshika", "Gupta",date1, propMapClient));
            clientRepository.save(keyspace,new Client(2L, "Sudhir", "Rao", propMapClient2));
            clientRepository.save(keyspace,new Client(5L,"Samiksha","Gupta",propMapClient3));

            // fetch all customers
            System.out.println("Clients found with findAll():");
            System.out.println("-------------------------------");
            for (Client customer : clientRepository.findAll(keyspace)) {
                System.out.println(customer);
            }

//            System.out.println(clientRepository.findById(1L));
            System.out.println("Select by one: ");
            ClientPrimaryKey id = new ClientPrimaryKey();
            id.setOrg_client_id(1L);
            id.setLast_updated_tstamp(date1);
            System.out.println(clientRepository.findById(keyspace,id));
        }
    }


    class UsersThread implements Runnable {

        private final String keyspace;

        public UsersThread(String keyspace) {
            this.keyspace = keyspace;
        }

        @Override
        public void run() {

            //Second thread accessing keyspace sample

            System.out.println("Thread-3");


            usersRepository.addKeyspace(keyspace);

            // fetch all customers
            System.out.println("Clients found with findAll():");
            System.out.println("-------------------------------");
            for (Users users : usersRepository.findAll()) {
                System.out.println(users);
            }
            System.out.println();

            System.out.println("Client found with findById(uuid):");
            System.out.println("--------------------------------");
            Users usersList = usersRepository.findById("0001");
            System.out.println(usersList);



        }
    }


    class CustomerThread implements Runnable {

        private final String keyspace;

        public CustomerThread(String keyspace) {
            this.keyspace = keyspace;
        }

        @Override
        public void run() {

            UUID uuid = new UUID(1,10);
            // save a couple of customers
            Map<String,Object> propMap = new HashMap<>();
            List<Long> numberList = new LinkedList<>();
            numberList.add(1234567589L);
            numberList.add(1234567598L);
            numberList.add(1235246789L);
            propMap.put("phone_numbers",numberList);
            UUID uuid1 = uuid.randomUUID();


            Map<String,Object> propMap2 = new HashMap<>();
            List<Map<String, Object>> paymentMethods = new LinkedList<>();
            Map<String, Object> payMap1 = new HashMap<>();
            payMap1.put("payment_method_code","452");
            payMap1.put("card_number",65451514);
            payMap1.put("date_from",new Date());
            payMap1.put("date_to",new Date());
            paymentMethods.add(payMap1);
            Map<String, Object> payMap2 = new HashMap<>();
            payMap2.put("payment_method_code","5414");
            payMap2.put("card_number",542124144);
            payMap2.put("date_from",new Date());
            payMap2.put("date_to",new Date());
            paymentMethods.add(payMap1);
            paymentMethods.add(payMap2);
            propMap2.put("payment_methods",paymentMethods);
            UUID uuid2 = uuid.randomUUID();

            Map<String,Object> propMap3 = new HashMap<>();
            Map<Date, Map<String, Object>> addressMap = new HashMap<>();
            Map<String, Object> address1 = new HashMap<>();
            address1.put("address_line1","abc");
            address1.put("address_line2","def");
            address1.put("address_line3","ghi");
            address1.put("city","LA");
            address1.put("postal_code",654114);
            addressMap.put(new Date(),address1);
            Map<String, Object> address2 = new HashMap<>();
            address2.put("address_line1","bhjb");
            address2.put("address_line2","kndskj");
            address2.put("address_line3","dkcnsdj");
            address2.put("city","Hyd");
            address2.put("postal_code",51514515);
            addressMap.put(new Date(),address2);
            propMap3.put("addresses",addressMap);
            UUID uuid3 = uuid.randomUUID();

//         First Thread accessing first keyspace : customer

            System.out.println("Thread - 4");

            customerRepository.addKeyspace(keyspace);

            customerRepository.deleteAll();

            customerRepository.save(new Customer(uuid1, "Alice", "Smith","alice.smith@gmail.com",propMap));
            customerRepository.save(new Customer(uuid2, "Bob", "Smith","bob12@gmail.com", propMap2));
            customerRepository.save(new Customer(uuid3,"Jana","NaJana","jana@gmail.com",propMap3));

            // fetch all customers
            System.out.println("Clients found with findAll():");
            System.out.println("-------------------------------");
            for (Customer customer : customerRepository.findAll()) {
                System.out.println(customer);
            }
            System.out.println();

            System.out.println("Client found with findById(uuid):");
            System.out.println("--------------------------------");
            Customer clientMasterList = customerRepository.findById(uuid2);
            System.out.println(clientMasterList);
        }
    }

}
