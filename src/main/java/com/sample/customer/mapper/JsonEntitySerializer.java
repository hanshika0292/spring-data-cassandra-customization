package com.sample.customer.mapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.sample.customer.annotations.CatchAll;
import org.springframework.data.mapping.model.IllegalMappingException;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;

/**
 * Created by hanshika on 9/9/16.
 */
public class JsonEntitySerializer extends JsonSerializer<Object> {

    @Override
    public void serialize(Object value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();
        for(Field field: value.getClass().getDeclaredFields()){
            Object val = null;
            field.setAccessible(true);
            try {
                val = field.get(value);
            } catch (IllegalAccessException e) {
                throw new IllegalMappingException("Expected a map CatchAll field",e);
            }
            if(field.isAnnotationPresent(JsonProperty.class)){
                gen.writeObjectField(field.getAnnotation(JsonProperty.class).value(),val);
            }else if(field.isAnnotationPresent(CatchAll.class)) {
                Map<String,Object> valMap = (Map<String,Object>) val;
                if (valMap != null) {
                    for (Map.Entry mapEntry : (valMap).entrySet()) {
                        gen.writeObjectField(String.valueOf(mapEntry.getKey()), mapEntry.getValue());
                    }
                }
            }
        }
        gen.writeEndObject();
    }


}
