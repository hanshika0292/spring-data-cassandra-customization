package com.sample.customer.mapper;

import com.datastax.driver.core.ColumnDefinitions;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.querybuilder.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.sample.customer.annotations.CatchAll;
import com.sample.customer.annotations.JsonEntity;
import jnr.ffi.annotations.In;
import org.springframework.core.CollectionFactory;
import org.springframework.data.cassandra.convert.BasicCassandraRowValueProvider;
import org.springframework.data.cassandra.convert.CassandraConverter;
import org.springframework.data.cassandra.convert.MappingCassandraConverter;
import org.springframework.data.cassandra.mapping.CassandraMappingContext;
import org.springframework.data.cassandra.mapping.CassandraPersistentEntity;
import org.springframework.data.cassandra.mapping.CassandraPersistentProperty;
import org.springframework.data.mapping.PersistentPropertyAccessor;
import org.springframework.data.mapping.model.ConvertingPropertyAccessor;
import org.springframework.data.mapping.model.IllegalMappingException;
import org.springframework.data.mapping.model.MappingException;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Created by hanshika on 9/9/16.
 */
public class ComplexMappingConverter extends MappingCassandraConverter {

    public ComplexMappingConverter(CassandraMappingContext cassandraMappingContext) {
        super(cassandraMappingContext);
    }

    @Override
    protected void readPropertyFromRow(CassandraPersistentEntity<?> entity, CassandraPersistentProperty property,
                                       BasicCassandraRowValueProvider row, PersistentPropertyAccessor propertyAccessor) {

        if (!(entity.isConstructorArgument(property)|| property.isCompositePrimaryKey())){
            if (!row.getRow().getColumnDefinitions().contains(property.getColumnName().toCql())) {
                if (property.isAnnotationPresent(CatchAll.class)) {
                    Object obj = getAnnotationTypeReadValue(row, propertyAccessor);
                    propertyAccessor.setProperty(property, obj);
                    return;
                }
            }
        }
        super.readPropertyFromRow(entity, property, row, propertyAccessor);
    }

    private Object getAnnotationTypeReadValue(BasicCassandraRowValueProvider row, PersistentPropertyAccessor propertyAccessor) {
        Map<String, Object> map = new HashMap<>();
        Field[] fields = propertyAccessor.getBean().getClass().getDeclaredFields();
        List<String> fieldNameList = new LinkedList<>();
        for (Field field : fields) {
            if (field.isAnnotationPresent(JsonProperty.class)) {
                fieldNameList.add(field.getAnnotation(JsonProperty.class).value());
            }
        }
        for (ColumnDefinitions.Definition definition : row.getRow().getColumnDefinitions()) {
            if (!fieldNameList.contains(definition.getName())) {
                map.put(definition.getName(), row.getRow().getObject(definition.getName()));
            }
        }
        return map;
    }

}
